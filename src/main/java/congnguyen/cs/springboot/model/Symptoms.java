package congnguyen.cs.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="SYPTOMS")
@Data
@ToString
@EqualsAndHashCode
public class Symptoms {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String idSymptoms;
	@Column(name = "NAME_SYPTOMS")
	private String nameSymptoms;
	@Column(name = "PROBABILITY")
	double[] Probability;
	public Symptoms(String idSymptoms, String nameSymptoms, double[] probability) {
		super();
		this.idSymptoms = idSymptoms;
		this.nameSymptoms = nameSymptoms;
		Probability = probability;
	}
}
