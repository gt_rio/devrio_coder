package congnguyen.cs.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Table(name="DISEASES")
@Data
@ToString
public class Diseases {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String idDiseases;
	
	@Column(name = "NAME_DISEASES")
	private String nameDiseases;
	@Column(name = "PROBABILITY")
	double[] Probability;
	public Diseases(String idDiseases, String nameDiseases, double[] probability) {
		super();
		this.idDiseases = idDiseases;
		this.nameDiseases = nameDiseases;
		Probability = probability;
	}
}
