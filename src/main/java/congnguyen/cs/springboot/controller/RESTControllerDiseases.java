/**
 * 
 */
package congnguyen.cs.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import congnguyen.cs.springboot.dao.DiseasesDAO;
import congnguyen.cs.springboot.model.Diseases;

/**
 * @author CongN
 * 2018-08-16
 */
@RestController("/api/diagnostic")
public class RESTControllerDiseases {
	@Autowired
	private DiseasesDAO diseasesDAO;
	
	//get AllDiseases
	@RequestMapping(value = "/listDis", method = RequestMethod.GET,
			produces = { MediaType.APPLICATION_JSON_VALUE,
						 MediaType.APPLICATION_XML_VALUE })
	public List<Diseases> getAllDiseases(){
		List<Diseases> list = diseasesDAO.getAllDiseases();
		return list;
	}
	//add Diseases
	@RequestMapping(value= "/{disId}", method = RequestMethod.POST,
			produces = { MediaType.APPLICATION_JSON_VALUE,
						 MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public Diseases addDiseases(@RequestBody Diseases diseases) {
		return diseasesDAO.addDiseases(diseases);
	}
	
	//delete diseases
	@RequestMapping(value= "/{disId}", method = RequestMethod.DELETE,
			produces = { MediaType.APPLICATION_JSON_VALUE,
						 MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public void deleteDiseases(@PathVariable("disId")String disId) {
		diseasesDAO.deleteDiseases(disId);
	}
	
	//update diseases
	@RequestMapping(value= "/update", method = RequestMethod.PUT,
			produces = { MediaType.APPLICATION_JSON_VALUE,
						 MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public Diseases updateDiseases(Diseases diseases) {
		return diseasesDAO.updateDiseases(diseases);
	}
	
	//get Diseases
	@RequestMapping(value= "/{disId}", method = RequestMethod.GET,
			produces = { MediaType.APPLICATION_JSON_VALUE,
						 MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public Diseases getDiseases(@PathVariable("disId")String disId) {
		return diseasesDAO.getDiseases(disId);
	}
}
