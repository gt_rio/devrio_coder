/**
 * 
 */
package congnguyen.cs.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import congnguyen.cs.springboot.dao.SymptomsDAO;
import congnguyen.cs.springboot.model.Symptoms;

/**
 * @author CongN
 * 2018-08-17
 */
@RestController("/api/syptoms")
public class RESTControllerSyptoms {
	
	@Autowired
	private SymptomsDAO symptomsDAO;
	
	//get AllSymptoms
	@RequestMapping(value = "/listSym", method = RequestMethod.GET,
			produces = {MediaType.APPLICATION_JSON_VALUE,
						MediaType.APPLICATION_XML_VALUE })
	public List<Symptoms> getAllSyptoms(){
		List<Symptoms> list = symptomsDAO.getAllSymptoms();
		return list;
	}
	
	//get Symptoms
	public Symptoms getSymptoms(@PathVariable("symId")String symId) {
		return symptomsDAO.getSymptoms(symId);
	}
	
	//add Symptoms
	@RequestMapping(value = "/{symId}", method = RequestMethod.POST,
			produces = { MediaType.APPLICATION_JSON_VALUE,
						 MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public Symptoms addSyptoms(@RequestBody Symptoms symptoms) {
		return symptomsDAO.addSyptoms(symptoms);
	} 
	
	//update Symptoms
	@RequestMapping(value = "/{symId}", method = RequestMethod.PUT,
			produces = { MediaType.APPLICATION_JSON_VALUE,
						 MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public Symptoms updateSymtoms(Symptoms symptoms) {
		return symptomsDAO.updateSyptoms(symptoms);
	}
	
	//delete Symptoms
	@RequestMapping(value = "/{symId}", method = RequestMethod.DELETE,
			produces = { MediaType.APPLICATION_JSON_VALUE, 
						 MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public void deleteSymptoms(@PathVariable("symId")String symId) {
		symptomsDAO.deleteSyptoms(symId);
	}
	
	
}
