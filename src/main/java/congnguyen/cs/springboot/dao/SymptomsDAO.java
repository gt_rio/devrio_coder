/**
 * 
 */
package congnguyen.cs.springboot.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import congnguyen.cs.springboot.model.Symptoms;

/**
 * @author CongN
 * 2018-08-17
 */
@Repository
public class SymptomsDAO {
	private static final Map<String, Symptoms> symMap = new HashMap<String, Symptoms>();
	
	static {
		initSyptoms();
	}

	/**
	 * init Object
	 */
	private static void initSyptoms() {
		// TODO Auto-generated method stub
		double[] probabilitySyptoms1 = {1, 0.9, 0.5};
		double[] probabilitySyptoms2 = {0.3, 0.4, 0.7};
		double[] probabilitySyptoms3 = {0.2, 0.8, 0.1};
		Symptoms symptoms1 = new Symptoms("1", "Headache", probabilitySyptoms1);
		Symptoms symptoms2 = new Symptoms("2", "Sneeze", probabilitySyptoms2);
		Symptoms symptoms3 = new Symptoms("3", "Glaucoma", probabilitySyptoms3);
		
		symMap.put(symptoms1.getIdSymptoms(), symptoms1);
		symMap.put(symptoms2.getIdSymptoms(), symptoms2);
		symMap.put(symptoms3.getIdSymptoms(), symptoms3);
	}
	
	public Symptoms getSymptoms(String symptomsId) {
		return symMap.get(symptomsId);
	}
	
	public static Map<String, Symptoms> getSymptoms(){
		return symMap;
	}
	
	public Symptoms updateSyptoms(Symptoms symptoms) {
		symMap.put(symptoms.getIdSymptoms(), symptoms);
		return symptoms;
	}
	
	public Symptoms addSyptoms(Symptoms symptoms) {
		symMap.put(symptoms.getIdSymptoms(), symptoms);
		return symptoms;
	}
	
	public void deleteSyptoms(String symptomsId) {
		symMap.get(symptomsId);
	}
	public List<Symptoms> getAllSymptoms(){
		Collection<Symptoms> collection = symMap.values();
		List<Symptoms> list = new ArrayList<Symptoms>();
		list.addAll(collection);
		return list;
	}
}
