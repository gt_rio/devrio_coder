/**
 * 
 */
package congnguyen.cs.springboot.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import congnguyen.cs.springboot.model.Diseases;

/**
 * @author CongN
 * 2018-08-16
 */
@Repository
public class DiseasesDAO {
	private static final Map<String, Diseases> diseasesMap = new HashMap<String,Diseases>();
	
	static {
		initDiagnostic();
	}

	/**
	 * init object
	 */
	private static void initDiagnostic() {
		// TODO Auto-generated method stub
		double[] probabilityDiseases1 = {0.3,0.5,0.6};
		double[] probabilityDiseases2 = {0.23,0.42,0.7};
		double[] probabilityDiseases3 = {1,0.01,0.9};
		
		Diseases diseases1 = new Diseases("11","Dementia",probabilityDiseases1);
		Diseases diseases2 = new Diseases("22","Fever",probabilityDiseases2);
		Diseases diseases3 = new Diseases("33","Diabetes",probabilityDiseases3);
		
		diseasesMap.put(diseases1.getIdDiseases(), diseases1);
		diseasesMap.put(diseases2.getIdDiseases(), diseases2);
		diseasesMap.put(diseases3.getIdDiseases(), diseases3);
	}
	
	public static Map<String, Diseases> getDisease(){
		return diseasesMap;
	}
	
	public Diseases getDiseases(String diseasesId) {
		return diseasesMap.get(diseasesId);
	}
	
	public Diseases addDiseases(Diseases diseases) {
		diseasesMap.put(diseases.getIdDiseases(),diseases);
		return  diseases;
	}
	
	public Diseases updateDiseases(Diseases diseases) {
		diseasesMap.put(diseases.getIdDiseases(), diseases);
		return diseases;
	}
	
	public List<Diseases> getAllDiseases(){
		Collection<Diseases> collectionDiseases = diseasesMap.values();
		List<Diseases> list  = new ArrayList<Diseases>();
		list.addAll(collectionDiseases);
		return list;
	}
	
	public void deleteDiseases(String diseasesId) {
		diseasesMap.remove(diseasesId);
	}
}
